# Introdução

## Equipes

O projeto foi desenvolvido pelo aluno de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Endryo Henrique| [@Endryo00](https://gitlab.com/Endryo00)|

# Documentação

A documentação do projeto pode ser acessada pelo link:

>  https://Endryo00.gitlab.io/ie21cp20201/

# Links Úteis
Site utilizado para conseguir o cógido da Marcha Imperial: 
https://dragaosemchama.com/en/2019/02/songs-for-arduino/

# Branch

 este texto foi criado na branch dev/web

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)
